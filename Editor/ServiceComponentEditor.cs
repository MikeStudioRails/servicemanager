﻿using ServiceDebugger;
using UnityEditor;
using UnityEngine;

namespace Service
{

    [CustomEditor(typeof(ServiceComponent))]
    public class ServiceComponentEditor : Editor
    {
        private const ushort MAX_DISPlAY_LENGTH = 1500;
        private const ushort IGNORE_LENGTH_LIMIT = 0;

        private const string OVERLIMIT_ERROR1 = "{0} is over {1} char.";
        private const string OVERLIMIT_ERROR2 = "Editing disabled";
        private const string COPY_TEXT = "Copy to Clipboard";

        private const string GUI_BTN_RESEND = "Resend";
        private const string GUI_BTN_RELEASE = "Release";
        private const string GUI_BTN_CANCEL = "Destroy";

        private const string GUI_LABEL_API = "Api:";
        private const string GUI_LABEL_STATE = "State:";
        private const string GUI_LABEL_SENTDATA = "SentData";
        private const string GUI_LABEL_RESPONSE = "Response";

        private Color orange = new Color(255, 165, 0);

        private byte SentDataKey = 0;
        private byte ResponseKey = 1;
        private string[] CappedText = new string[2];


        public override void OnInspectorGUI()
        {
            ServiceComponent request = target as ServiceComponent;
            EditorStyles.textField.wordWrap = true;

            switch(request.CurrentState)
            {
                case ServiceComponent.REQUEST_STATE.Waiting:
                    GUI.backgroundColor = Color.yellow;
                    break;
                case ServiceComponent.REQUEST_STATE.InProgress:
                    GUI.backgroundColor = orange;
                    break;
                case ServiceComponent.REQUEST_STATE.Failed:
                    GUI.backgroundColor = Color.red;
                    break;
                case ServiceComponent.REQUEST_STATE.FrozenPostResponse:
                case ServiceComponent.REQUEST_STATE.FrozenPreResponse:
                    GUI.backgroundColor = Color.cyan;
                    break;
            }

            //Show current state
            EditorGUILayout.LabelField(GUI_LABEL_STATE, request.CurrentState.ToString(), EditorStyles.boldLabel);

            //Show api call
            EditorGUILayout.LabelField(GUI_LABEL_API);
            request.API = EditorGUILayout.TextField(request.API);

            if(!request.HistoryDeleted)
            {

                DrawUILine(Color.grey);

                //Display the data sent to server 
                request.SentData = CapTextarea(SentDataKey, GUI_LABEL_SENTDATA, request.SentData, MAX_DISPlAY_LENGTH);

                DrawUILine(Color.grey);

                if(request.CurrentState == ServiceComponent.REQUEST_STATE.Failed || request.CurrentState == ServiceComponent.REQUEST_STATE.Succeeded)
                {
                    //Display the data recieved form server
                    request.Response = CapTextarea(ResponseKey, GUI_LABEL_RESPONSE, request.Response, MAX_DISPlAY_LENGTH);
                    DrawUILine(Color.grey);
                }

                //If the current API call is frozen then show a button to cancel or unfreeze
                if(request.CurrentState == ServiceComponent.REQUEST_STATE.FrozenPostResponse ||
                    request.CurrentState == ServiceComponent.REQUEST_STATE.FrozenPreResponse)
                {
                    if(GUILayout.Button(GUI_BTN_CANCEL))
                    {
                        request.CancelCall();
                    }
                    if(GUILayout.Button(GUI_BTN_RELEASE))
                    {
                        request.ReleaseFrozenCall();
                    }
                }
                else
                {
                    //Otherwise display a resend api button
                    GUI.backgroundColor = Color.green;
                    if(GUILayout.Button(GUI_BTN_RESEND))
                    {
                        request.Resend();
                    }
                }
            }
            DrawUILine(Color.black, 15, 15);

        }

        private string CapTextarea(byte id, string labelTitle, string data, ushort maxDisplayLength)
        {
            if(data != null)
            {
                EditorGUILayout.LabelField(labelTitle);

                //Add a copy to clipboard button
                if(GUILayout.Button(COPY_TEXT))
                {
                    EditorGUIUtility.systemCopyBuffer = data;
                }

                if(maxDisplayLength == IGNORE_LENGTH_LIMIT || data.Length <= maxDisplayLength)
                {
                    //if text isn't too long, display and return any changes
                    return EditorGUILayout.TextArea(data, GUILayout.MaxHeight(100));
                }
                else
                {
                    //cache capped copy
                    if(id > CappedText.Length)
                    {
                        SDebug.LogError("KEY IS LARGER THAN ARRAY LENGTH.");
                    }

                    if(CappedText[id] == null)
                    {
                        //Editing is disabled, so we can presume this data won't change
                        CappedText[id] = string.Concat(data.Substring(0, maxDisplayLength), "...");
                    }

                    //if text is longer than specified limit, display capped version and show error message
                    EditorGUILayout.LabelField(string.Format(OVERLIMIT_ERROR1, labelTitle, maxDisplayLength));
                    GUI.color = Color.red;
                    EditorGUILayout.LabelField(string.Format(OVERLIMIT_ERROR2, labelTitle, maxDisplayLength), EditorStyles.whiteBoldLabel);
                    GUI.color = Color.white;
                    EditorGUILayout.TextArea(CappedText[id], GUILayout.MaxHeight(50));
                }
            }
            return data;
        }

        private void DrawUILine(Color color, int thickness = 2, int padding = 10)
        {
            Rect r = EditorGUILayout.GetControlRect(GUILayout.Height(padding + thickness));
            r.height = thickness;
            r.y += padding / 2;
            r.x -= 2;
            r.width += 6;
            EditorGUI.DrawRect(r, color);
        }
    }
}