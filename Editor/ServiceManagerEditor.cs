﻿using ServiceDebugger;
using UnityEditor;
using UnityEngine;

namespace Service
{

    /// <summary>
    /// Creates buttons to interact with ServiceManager helper methods.
    /// </summary>

    [CustomEditor(typeof(ServiceManager))]
    public class ServiceManagerEditor : Editor
    {

        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            ServiceManager launcher = target as ServiceManager;

            if(GUILayout.Button("Log History"))
            {
                Debug.Log(launcher.GetHistoryAsString());
            }

            if(GUILayout.Button("Clear History"))
            {
                launcher.ClearHistory();
            }

            if(launcher.FreezeApiCommunication)
            {
                if(GUILayout.Button("Turn off api freeze"))
                {
                    launcher.FreezeApiCommunication = false;
                }
            }
            else
            {
                if(GUILayout.Button("Turn on api freeze"))
                {
                    launcher.FreezeApiCommunication = true;
                }
            }

        }

    }
}