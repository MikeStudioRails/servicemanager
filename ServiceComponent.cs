﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ServiceDebugger;
using UnityEngine.Networking;

/// <summary>
/// A new ServiceComponent is created for each request, it encapsulates all the requerst related data
/// and fires a callback when the request is finished
/// 
/// The callback uses a generic class type derived from 'ServerResponse', and uses Unitys JsonUtility.FromJson
/// to map the data directly.
/// </summary>
 

namespace Service
{
    public class ServiceComponent : MonoBehaviour
    {
        private const int TIMEOUT_SECONDS = 15;

        private const string HISTORY_BASE_STRING = "{0} - {1} \n{2} \nTime taken:{3}:{4} \nSent data: {5}\n\n";
        private const string HISTORY_DATE_FORMAT = "hh:mm:ss";
        private const string NEWLINE = "\n";
        private const int HISTORY_STRING_PADDING = 40;// 40 is a ballpark figure, overestimating additional text, value lengths and padding

        public enum REQUEST_STATE
        {
            Waiting,
            InProgress,
            Succeeded,
            Failed,
            FrozenPreResponse,
            FrozenPostResponse,
            Cancelled
        }

        public REQUEST_STATE CurrentState;

        public bool Active = false;

        public int ID = 0;
        public string API = "", SentData = "", Response = "";

        public DateTime Created;
        public TimeSpan TimeTaken = new TimeSpan(0);

        private string url;
        private UnityWebRequest webRequest;
        private ServiceCallbackBase Callback;
        private Dictionary<string, string> headers;
        private ErrorCodes.ERROR Error = ErrorCodes.ERROR.NONE;

        private bool freezeCall;
        private bool keepDeepHistory;
        private bool useByteArray = true; // Always true currenly, TODO add ability to switch

        public bool HistoryDeleted { get; private set; }

        /// <summary>
        /// Setup assigns all the data needed to amke the server call, this method doesn't start the call.
        /// </summary>
        /// <typeparam name="T">The Response class that matches the json response, must derive ServiceCallback.cs </typeparam>
        public void Setup<T>(int id, string url, string api, Dictionary<string, string> headers, ServiceDictionary pushData, bool keepDeepHistory, Action<T> responseCallback, Action<ServerResponse> errorCallback) where T : ServerResponse
        {
            //map data across
            ID = id;
            API = api;
            CurrentState = REQUEST_STATE.Waiting;

            this.url = url;
            this.headers = headers;
            this.keepDeepHistory = keepDeepHistory;

            if(pushData != null)
            {
                SentData = pushData.GetJson();
            }

            //setup callback
            if(responseCallback != null)
            {
                Callback = new ServiceCallback<T>(responseCallback, errorCallback);
            }
            else if(errorCallback != null)
            {
                Callback = new ServiceCallbackBase(errorCallback);
            }
        }

        /// <summary>
        /// Apply request freezing, this allows modifications to the sent or recieved data before processing.
        /// </summary>
        public void SetFrozenState(bool state)
        {
            freezeCall = state;
        }

        /// <summary>
        /// After calling setup(), use Fire to start the www call
        /// </summary>
        public void Fire()
        {
            Active = true;
            Created = DateTime.Now;
            CurrentState = REQUEST_STATE.InProgress;

            if(!freezeCall)
            {
                TriggerRequestRoutine();
            }
            else
            {
                SDebug.LogWarning("Service is Frozen - Pre request");
                CurrentState = REQUEST_STATE.FrozenPreResponse;
            }
        }

        private void TriggerRequestRoutine()
        {
            if(Application.internetReachability == NetworkReachability.NotReachable)
            {
                HandleFailedRequest(ErrorCodes.ERROR.NO_CONNECTION, "Cannot connect server, check your internet");
            }
            else
            {
                StartCoroutine(StartRequest());
            }
        }

        /// <summary>
        /// This Coroutine handles the starting and monitoring the server connection
        /// </summary>
        private IEnumerator StartRequest()
        {
            string reqUrl = url + API;

            SDebug.Log("Start server Request!\n", reqUrl, "\n", SentData);

            //Grab either a Get or Post web request
            if(!string.IsNullOrEmpty(SentData))
            {
                if(useByteArray)
                {
                    //put allows untampered byte Data
                    webRequest = UnityWebRequest.Put(reqUrl, System.Text.Encoding.UTF8.GetBytes(SentData));
                    webRequest.method = UnityWebRequest.kHttpVerbPOST;
                }
                else
                {
                    webRequest = UnityWebRequest.Post(reqUrl, SentData);
                }
            }
            else
            {
                webRequest = UnityWebRequest.Get(reqUrl);
            }

            foreach(KeyValuePair<string, string> item in headers)
            {
                webRequest.SetRequestHeader(item.Key, item.Value);
            }

            webRequest.timeout = TIMEOUT_SECONDS;

            //Send the web request and wait for response
            yield return webRequest.SendWebRequest();

            //Handle response
            if(webRequest.isNetworkError)
            {
                HandleFailedRequest(ErrorCodes.ERROR.SERVER_ERROR, webRequest.error + NEWLINE + webRequest.downloadHandler.text);
            }
            else if(webRequest.isHttpError)
            {
                HandleFailedRequest(ErrorCodes.CodeToError(webRequest.responseCode), webRequest.error + NEWLINE + webRequest.downloadHandler.text);
            }
            else
            {
                HandleSuccessfulRequest(webRequest.downloadHandler.text);
            }

            //dispose of the webrequest when finished
            webRequest.Dispose();
        }

        /// <summary>
        /// Handles a successful server request
        /// </summary>
        private void HandleSuccessfulRequest(string jsonResponse)
        {
            if(Active)
            {
                Active = false;
                Response = jsonResponse;

                SDebug.Log("Request Successful: ", API);

                if(!freezeCall)
                {
                    SendSuccessfulCallback();
                }
                else
                {
                    SDebug.LogWarning("Service is Frozen - Post Response");
                    CurrentState = REQUEST_STATE.FrozenPostResponse;
                }
            }
        }

        /// <summary>
        /// Invokes the assigned callback and sets request as successful
        /// </summary>
        private void SendSuccessfulCallback()
        {
            InvokeCallback(true);
        }

        /// <summary>
        /// Marks request as failed and invokes appropriate callback
        /// </summary>
        /// <param name="errorType">States the type of Error</param>
        /// <param name="errorText">Detailed description of what went wrong</param>
        public void HandleFailedRequest(ErrorCodes.ERROR errorType, string errorText = "")
        {
            SDebug.Log("Request Failed: ", API, errorType.ToString());

            Error = errorType;
            Response = errorText;

            InvokeCallback(false);
        }

        private void InvokeCallback(bool successful)
        {
            CurrentState = successful ? REQUEST_STATE.Succeeded : REQUEST_STATE.Failed;

            if(Callback != null)
            {
                if(!Callback.HandleCallback(successful, Error, Response) && successful)
                {
                    // Something went wrong
                    HandleFailedRequest(ErrorCodes.ERROR.BAD_RESPONSE_DATA, "See Error Log");
                }
            }

            CleanUp();
        }

        /// <summary>
        /// Direct mwthod to unfreeze the request if currently frozen
        /// </summary>
        public void ReleaseFrozenCall()
        {
            if(CurrentState == REQUEST_STATE.FrozenPreResponse)
            {
                TriggerRequestRoutine();
            }
            else if(CurrentState == REQUEST_STATE.FrozenPostResponse)
            {
                SendSuccessfulCallback();
            }
        }

        /// <summary>
        /// Cancels the current request and runs the cleans up method
        /// </summary>
        public void CancelCall()
        {
            CurrentState = REQUEST_STATE.Cancelled;
            CleanUp();
        }

        /// <summary>
        /// Resends the api request.
        /// </summary>
        public void Resend()
        {
            if(keepDeepHistory)
            {
                SDebug.LogWarning("Resending api calls have no callback");
                enabled = true;
                Fire();
            }
            else
            {
                SDebug.LogError("Cannot Resend service request while keepDeepHistory is false");
            }
        }

        /// <summary>
        /// Returns an estimated length of the components data as a string
        /// </summary>
        public int GetHistoryStringLength()
        {
            return (API.Length + HISTORY_BASE_STRING.Length + HISTORY_STRING_PADDING + (SentDataAvailible() ? SentData.Length : 0));
        }

        /// <summary>
        /// Returns a string representation of the request
        /// </summary>
        public string GetRequestAsHistoryString()
        {
            return string.Format(HISTORY_BASE_STRING, Created.ToString(HISTORY_DATE_FORMAT), API, CurrentState.ToString(), TimeTaken.TotalSeconds, TimeTaken.Milliseconds, SentDataAvailible() ? SentData : "");
        }

        private bool SentDataAvailible()
        {
            return (keepDeepHistory && !HistoryDeleted);
        }

        /// <summary>
        /// Stops any processes and disposes current web request
        /// if keepDeepHistory is false then deallocates strings
        /// </summary>
        private void CleanUp()
        {
            if(Active)
            {
                Active = false;
                StopAllCoroutines();

                if(webRequest != null)
                {
                    webRequest.Dispose();
                }

                TimeTaken = (DateTime.Now - Created);
            }

            if(Callback != null)
            {
                Callback.ClearCallback();
                Callback = null;
            }

            if(!keepDeepHistory)
            {
                SentData = Response = url = null;
                headers = null;
                HistoryDeleted = true;
            }

            enabled = false;
        }

    }
}