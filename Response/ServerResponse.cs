﻿using System;

namespace Service
{
    [Serializable]
    public class ServerResponse
    {
        public bool Errored = false;
        public ErrorCodes.ERROR ErrorType = ErrorCodes.ERROR.NONE;
        public string Error;
    }
}