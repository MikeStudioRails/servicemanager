# ServiceManager

> Please note this manager has been recently created and used in a limited testing enviroment, future updates will address the limitations.

Service manager is a class used for json based communication In Unity.

Each request creates a new ServiceComponent.cs that can be tracked, edited and resent during runtime.

During production builds it is recommended that 'keepDeepHistory' is turned off and 'ClearHistory()' is called during scene changes.

Ensure you're using the [Serializable] tag on Callback classes.


####Example use case
```cs
Dictionary<string, string> headers = new Dictionary<string, string>();
headers.Add("Content-Type", "application/json");

ServiceManager.Instance.SetServer("https://YourServer.com/ ", headers, Application.isEditor);
```

```
ServiceDictionary pushData = new ServiceDictionary();
pushData.Add("username", User);
pushData.Add("password", Pass);

ServiceManager.Instance.ContactServer<ModuleResponse>(successCallback, errorCallback, "download/latest/", pushdata); 
```

The service manager will use Unitys JsonUtility.FromJson to create an object from "ModuleResponse" in this situation. Please be aware of Unitys serailization limitations.

However with minimal effort a the callback could return a raw string which you can pass to your parser of choice.


###Images

Example of successful request component with full history enabled:

![](https://gitlab.com/MikeStudioRails/servicemanager/raw/master/ServiceManager.PNG)





