﻿using System;
using System.Collections.Generic;
using UnityEngine;
using ServiceDebugger;
using System.Text;

/// <summary>
/// Service manager is a class used for json based communication. 
/// Each requests creates a new ServiceComponenet.cs (This handles the server requests)
/// During production builds it is reccomended that keepDeepHistory is turned off and ClearHistory() is called during scene changes.
/// 
/// Ensure you're using the [Serializable] tag on Callback classes.
/// </summary>


//TODO Add offline/stub request support
//TODO Add more support for different callbacks options (that don't rely on JsonUtility.FromJson) 
//TODO Decouple ServiceComponent and allow multiple variations to be used based on interfaces

namespace Service
{

    public class ServiceManager : MonoBehaviour
    {

        //-- Start Singelton code
        private static ServiceManager instance;

        public static ServiceManager Instance
        {
            get
            {
                if(instance == null)
                {
                    GameObject parent = new GameObject("ServiceManager");
                    instance = parent.AddComponent<ServiceManager>();
                    DontDestroyOnLoad(parent);
                }
                return instance;
            }
        }
        //-- End Singelton code

        private string serverBaseURL = "";
        private Dictionary<string, string> headerData;
        private List<ServiceComponent> serverComponents = new List<ServiceComponent>();

        private int serverID = 0;
        private bool freezeApiCommunication = false;
        private bool keepDeepHistory = false;

        public bool FreezeApiCommunication
        {
            get { return freezeApiCommunication; }
            set
            {
                if(freezeApiCommunication != value)
                {
                    freezeApiCommunication = value;
                    foreach(ServiceComponent item in serverComponents)
                    {
                        item.SetFrozenState(freezeApiCommunication);
                    }
                }
            }
        }

        /// <summary>
        /// Set Server URL and assign header Data
        /// </summary>
        /// <param name="newurl">The url of the server taking requests</param>
        /// <param name="headerData">Any header data required by server/param>
        /// <param name="keepHistory">Turn on while debugging, use Application.isEditor to see history in Editor only
        /// 
        public void SetServer(string newUrl, Dictionary<string, string> headerData, bool keepDeepHistory = false)
        {
            serverBaseURL = newUrl;
            this.headerData = headerData;
            this.keepDeepHistory = keepDeepHistory;
        }

        /// <summary>
        /// Detail which logs types are used or ignored.
        /// Multiple method calls, reset previously set permissions
        /// </summary>
        /// <param name="permissions">Supply an array of permissions</param>
        public void SetLogPermissions(params SDebug.LogType[] permissions)
        {
            SDebug.SetEnabledLogs(permissions);
        }

        /// <summary>
        /// Creates a new API request in the form of a component attached to gameobject.
        /// </summary>
        /// <typeparam name="T">The class type to map response data to, must derive 'ServerResponse' </typeparam>
        /// <param name="callback">The method to callback on successful response.</param>
        /// <param name="errorCallback">The method to callback on failed response.</param>
        /// <param name="api">The sub url api to append on server.</param>
        /// <param name="pushData">Any data to be sent with request</param>
        /// <param name="allowMultipleCalls">Can there be multiples requests processing at the same time.</param>
        /// <returns></returns>
        public int ContactServer<T>(Action<T> callback, Action<ServerResponse> errorCallback, string api, ServiceDictionary pushData, bool allowMultipleCalls = false) where T : ServerResponse
        {
            //Create a new ServiceComponent
            ServiceComponent serverCall = gameObject.AddComponent<ServiceComponent>();

            serverCall.Setup(serverID++ ,serverBaseURL, api, headerData, pushData, keepDeepHistory, callback, errorCallback);
            serverCall.SetFrozenState(FreezeApiCommunication);

            serverComponents.Add(serverCall as ServiceComponent);

            if(!allowMultipleCalls && ActiveApiCallExists(api))
            {
                serverCall.HandleFailedRequest(ErrorCodes.ERROR.DUPLICATE);
            }
            else
            {
                serverCall.Fire();
            }

            return serverCall.ID;
        }

        /// <summary>
        /// Helper function to check if a api request is currently being processed
        /// </summary>
        /// <param name="api"></param>
        /// <returns></returns>
        public bool ActiveApiCallExists(string api)
        {
            foreach(ServiceComponent entry in serverComponents)
            {
                if(entry.Active && entry.API == api)
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Cancels all the active requests being processed.
        /// Keep in mind, that data will already have been sent to the server.
        /// </summary>
        public void CancelAllRequests()
        {
            foreach(ServiceComponent entry in serverComponents)
            {
                if(entry.Active)
                {
                    entry.CancelCall();
                }
            }
        }

        /// <summary>
        /// Cancels all the active requests being processed that target the api param.
        /// Keep in mind, that data will already have been sent to the server.
        /// </summary>
        /// <param name="api"></param>
        public void CancelAllCallsOf(string api)
        {
            foreach(ServiceComponent entry in serverComponents)
            {
                if(entry.Active && entry.API == api)
                {
                    entry.CancelCall();
                }
            }
        }

        /// <summary>
        /// Cancels the request associated with the ID.
        /// </summary>
        /// <param name="id"></param>
        public void CancelCall(int id)
        {
            foreach(ServiceComponent entry in serverComponents)
            {
                if(entry.Active && entry.ID == id)
                {
                    entry.CancelCall();
                }
            }
        }

        /// <summary>
        /// </summary>
        /// <returns> Returns the sessions service history (without responses)</returns>
        public string GetHistoryAsString()
        {
            StringBuilder history;
            int strLength = 0;

            //find the total length of components history strings
            foreach(ServiceComponent entry in serverComponents)
            {
                strLength += entry.GetHistoryStringLength();
            }

            //assign and fill the stringbuilder representing the history data
            history = new StringBuilder(strLength);
            foreach(ServiceComponent entry in serverComponents)
            {
                history.Append(entry.GetRequestAsHistoryString());
            }

            return history.ToString();
        }

        /// <summary>
        /// Clear recorded history
        /// </summary>
        public void ClearHistory()
        {
            for(int i = serverComponents.Count - 1; i >= 0; i--)
            {
                if(!serverComponents[i].Active)
                {
                    Destroy(serverComponents[i]);
                    serverComponents.RemoveAt(i);
                }
            }

        }
        
    }
}