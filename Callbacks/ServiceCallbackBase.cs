﻿using System;

namespace Service
{
    public class ServiceCallbackBase
    {
        private Action<ServerResponse> ErrorCallback;

        public ServiceCallbackBase(Action<ServerResponse> errorCallback)
        {
            ErrorCallback = errorCallback;
        }

        /// <summary>
        /// </summary>
        /// <returns>Returns if callback was successful</returns>
        public bool HandleCallback(bool success, ErrorCodes.ERROR errorType, string data)
        {
            if(success)
            {
                return FireSuccessfulCallback(data);
            }
            else
            {
                return FireErrorCallback(errorType, data);
            }
        }

        protected virtual bool FireSuccessfulCallback(string data)
        {
            return true;
        }

        protected bool FireErrorCallback(ErrorCodes.ERROR errorType, string error)
        {
            ServerResponse errorResponse = new ServerResponse();
            errorResponse.Errored = true;
            errorResponse.ErrorType = errorType;
            errorResponse.Error = error;

            ErrorCallback.Invoke(errorResponse);

            return true;
        }

        public virtual void ClearCallback()
        {
            ErrorCallback = null;
        }
    }
}
