﻿using System;
using UnityEngine;
using ServiceDebugger;

namespace Service
{
    /// <summary>
    /// Extends the ServiceCallbackBase and adds generic json mapping
    /// </summary>
    /// <typeparam name="T">Must derive ServerResponse and have the [Serialized] tag.</typeparam>

    public class ServiceCallback<T> : ServiceCallbackBase where T : ServerResponse
    {
        private Action<T> Callback;

        public ServiceCallback(Action<T> callback, Action<ServerResponse> errorCallback) : base(errorCallback) 
        {
            Callback = callback;
        }

        protected override bool FireSuccessfulCallback(string data)
        {
            try
            {
                Callback.Invoke(JsonUtility.FromJson<T>(data));
                return true;
            }
            catch(Exception e)
            {
                SDebug.LogError("Failed Json Conversion " + e.ToString());
                return false;
            }
        }

        public override void ClearCallback()
        {
            Callback = null;
            base.ClearCallback();
        }
    }
}
