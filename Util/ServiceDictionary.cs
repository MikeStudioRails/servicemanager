﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using ServiceDebugger;

/// <summary>
/// Custom dictionary for easy conversion to json
/// </summary>
///

//TODO Current has basic functionality, it doesn't support many types currently and doesn't support complex lists
//TODO Implement a recursive design to target embeded types
//TODO test with more data types

//TODP Probably easier to use a pre-existing serialiazable dictionary or thirdparty library

namespace Service
{

    [Serializable]
    public class ServiceDictionary : Dictionary<string, object>, IJsonObject
    {

        public ServiceDictionary() : base() { }

        public string GetJson()
        {
            string json = "{", jsonconnector = "";

            string entryValue;
            bool ignoreQuotations = false;

            Type entryType;
            TypeCode subTypeCode;
            TypeCode entryTypeCode;

            //Loop dictionary and build json string
            foreach(KeyValuePair<string, object> item in this)
            {
                entryValue = "";
                entryType = item.Value.GetType();
                entryTypeCode = Type.GetTypeCode(entryType);
                Type[] listInterfaces = entryType.GetInterfaces();
                ignoreQuotations = false;

                //Convert entry data to string representation
                try
                {
                    if(entryTypeCode == TypeCode.String)
                    {
                        entryValue = (string)item.Value;
                    }
                    else if(IsNumeric(entryTypeCode))
                    {
                        ignoreQuotations = true;
                        entryValue = item.Value.ToString();
                    }
                    else if(listInterfaces.Contains(typeof(IJsonObject)))
                    {
                        entryValue = ((IJsonObject)item.Value).GetJson();
                        ignoreQuotations = true;
                    }
                    else if(listInterfaces.Contains(typeof(IEnumerable)))
                    {
                        ignoreQuotations = true;
                        entryValue = "[";
                        subTypeCode = Type.GetTypeCode(entryType.GetGenericArguments().Single());

                        //TODO check for element type of an enumerable and handle accordingly
                        foreach(var i in (IEnumerable)item.Value)
                        {
                            if(IsNumeric(subTypeCode))
                            {
                                entryValue += i.ToString() + ",";
                            }
                            else
                            {
                                entryValue += "\"" + i.ToString() + " \",";
                            }
                        }

                        entryValue = entryValue.Substring(0, entryValue.Length-1) + "]";
                    }
                    else if(entryType.IsSerializable)
                    {
                        entryValue = JsonUtility.ToJson(item.Value);
                    }
                    else
                    {
                        entryValue = item.Value.ToString();
                    }

                }
                catch(Exception e)
                {
                    SDebug.LogError("Error converting Service Dictionary: " + e.ToString());
                    entryValue = "";
                }

                if(ignoreQuotations)
                {
                    json = string.Concat(json, jsonconnector, "\"", item.Key.ToString(), "\":", entryValue);
                }
                else
                {
                    json = string.Concat(json, jsonconnector, "\"", item.Key.ToString(), "\":\"", entryValue, "\"");
                }

                jsonconnector = ",";
            }

            return string.Concat(json, "}");
        }

        private bool IsNumeric(TypeCode code)
        {
            switch(code)
            {
                case TypeCode.Byte:
                case TypeCode.SByte:
                case TypeCode.UInt16:
                case TypeCode.UInt32:
                case TypeCode.UInt64:
                case TypeCode.Int16:
                case TypeCode.Int32:
                case TypeCode.Int64:
                case TypeCode.Decimal:
                case TypeCode.Double:
                case TypeCode.Single:

                    return true;
                default:
                    return false;
            }
        }

    }
}