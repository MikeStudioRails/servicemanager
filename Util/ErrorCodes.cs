﻿
namespace Service
{
    public static class ErrorCodes
    {
        private const int HTTPCODE_404 = 404;
        private const int HTTPCODE_500 = 500;

        //CHANGE PER SERVER
        private const int HTTPCODE_UPGRADE = 301;
        private const int HTTPCODE_MAINTENANCE = 503;

        public enum ERROR
        {
            NONE,
            NO_CONNECTION,
            TIMEOUT,
            SERVER_ERROR,
            UNKNOWN_ERROR,
            API_NOT_FOUND,
            UPGRADE,
            SERVER_MAINTENANCE,
            DUPLICATE,
            BAD_RESPONSE_DATA
        }

        public static ERROR CodeToError(long code)
        {
            switch(code)
            {
                case HTTPCODE_404:
                    return ERROR.API_NOT_FOUND;

                case HTTPCODE_500:
                    return ERROR.SERVER_ERROR;

                case HTTPCODE_UPGRADE:
                    return ERROR.UPGRADE;

                case HTTPCODE_MAINTENANCE:
                    return ERROR.SERVER_MAINTENANCE;

                default:
                    return ERROR.UNKNOWN_ERROR;
            }
        }

    }
}